1. Create the board
    a. Build a 7x6 column grid
        1. Create a nested loop to create columns and rows


2. Make gaming pieces move
    a. Be able to switch player mode
        - Create a click handler (2) to be able to copy a gaming piece from the side to the inside of the board 

    
3. Set up winning conditions (horizontal, vertical, diagonal)
    a. Alert when a winning condition has been met for either player

Group Members:

1. April Limas
2. Meshelle Merritt
3. Charles Campbell
4. Caitlin Golden

Play Connect Four here: https://april_limas.gitlab.io/connectfour